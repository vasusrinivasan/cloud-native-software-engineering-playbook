# Make friends with Inner and Outer Joins

Learn to use the correct join for the correct situation, by modeling if you need
- A U B (Union)
- A n B (Intersection)
- A U B'
- A n B'
- (A n B)' 
For instance use the following:
- Use Inner Join for A U B.
- Use Full Outer Join for A n B.
- Use Left Outer Join for A U B'
- Use Left Outer Join where B.id is null for A n B'
- Use Full Outer Join where A.id and B.id is null for (A n B)'
