# REST API Pagination

- Use pagination when there are lots of results to return in your REST API

- When there are lots of data to return, the GET method of the particular endpoint should have a limit such as https://acme.com/v2/employees/page?limit=15 
- If no limit is specified a default limit is assumed.
- In the response to such a request, two links should be returned, one link is prev which has the link to the previous page and the other link is next which has the link to the next page.
- The first page will not have a prev link and the last page will not have the next link.
- Optionally, there could be an entity called pages which contains the total number of pages.
