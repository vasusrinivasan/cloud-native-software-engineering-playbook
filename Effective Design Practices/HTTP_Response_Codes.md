# HTTP Response codes

Use standard HTTP Response codes to indicate the outcome of the response.
- HTTP Code 200 OK: To indicate that the response was successful. 
- HTTP Code 201 Resource Created: When the resource was successfully created.
- HTTP Code 202 Request Accepted: When the request was received but not completed.
- HTTP Code 204 No Content: When the request was fulfilled but does not return a response body.
- HTTP Code 206 Partial Content : When the Range header is used by the client to request partial content.
- HTTP Code 400 Bad Request: When the request could not be understood by the server due to bad syntax.
- HTTP Code 401 Unauthorized: When the request requires an authorization header.
- HTTP Code 403 Forbidden: When the request is unauthorized;the authorization header is invalid.
- HTTP Code 404 Not Found : When the resource was not found.
- HTTP Code 405 Method Not Allowed: When the corresponding HTTP method was not allowed. 
- HTTP Code 500 Internal Server Error: When the Server throws an Exception.
