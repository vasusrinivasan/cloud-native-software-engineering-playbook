# Document the rationale behind Engineering Design Decisions

Clearly document why you choose a particular option over several options and checkin the document into the source code repository so that it is useful for enhancement or maintenance.