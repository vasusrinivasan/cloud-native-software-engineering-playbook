# Retry while consuming REST APIs

- Whenever your service is a consumer to external REST API/Web Services, always assume that the network is unreliable and design arround it.

- When a connection to an external web service is unsuccessful, design a reconnection logic so that, you try it again.

- But, do not immediately try, rather try after a soaking period that has an exponential backoff. 

- Exponential backoff is an algorithm that uses feedback to multiplicatively decrease the retry rate, in order to gradually find an acceptable rate. 

- Also, have a timeperiod after which your service won't try to retry consuming a web service. Otherwise, your service might get caught into infinite retry loops.