# Avoid the N+1 Problem in your REST API

- Do not require clients to your REST API to call the server N+1 times to fetch one collection resource + N child resources.

- Instead, include enough information in a single resource inside collection resources rather than making the client call the server many times unnecessarily.

- For instance the employee entity might have N child resources such as department, address, joining information, skill set etc., 

- Include all relevant information in a single endpoint /employee rather than requiring the client make N calls.
