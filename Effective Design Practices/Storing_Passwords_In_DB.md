# Storing Passwords in the database

Do not store a password as plaintext in the database. Instead do the following:

- Randomly generate a “salt” string.

- Concatenate the generated salt string with the password.

- Use a modern “slow” one-way hashing function to hash the password+salt string.

- Store this one-way hash along with the salt string.

- When you receive the user’s password, retrieve the salt stored as plain text in the database and concatenate with the password.

- Create the one-way hash using the same function that you used on the password+salt string.

- Compare the hash with the stored password hash in the database.
If it is a match, let the user in.
