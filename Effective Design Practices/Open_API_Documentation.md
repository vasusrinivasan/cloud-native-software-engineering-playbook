# Use Swagger/Open API for documentation

- When you design a REST API, define it using Open API and check it in to your source code repository.

- Most IDEs have support to automatically generate Open API documentation. So use them to generate API Documentation for existing microservices that do not have API documentation.