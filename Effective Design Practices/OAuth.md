# Choose OAuth 2.0 over JWT

Read this paper for evidence. Many major enterprise providers including Zoom are deprecating JWT and are choosing OAuth 2.0.

https://redis.com/docs/json-web-tokens-jwts-are-not-safe/?code=oauth&utm_source=google&utm_medium=cpc&utm_term=oauth&utm_campaign=growth-jwt-us-16719312883&utm_content=jwt_are_not_safe&gclid=CjwKCAjwpKyYBhB7EiwAU2Hn2X92Oc_YRVKaZf84Vl-cJJ8ry3SK-vFUh96tDLlEATanaU8VvbODbxoCfqAQAvD_BwE 