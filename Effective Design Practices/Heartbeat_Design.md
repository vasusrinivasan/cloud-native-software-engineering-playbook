# Heartbeat design

Whenever your clients need to send heartbeats to your server, design so that, the heartbeats do not happen from hundreds or thousands of clients all the same time, thereby overwhelming your service.

- Rather design in such a way, so that, you give a scheduled time window in which your client will ideally reach you.

- If your client could not reach you in the time window, allow them, yet give a "defaulter" response back.

- Also have a scheduler to maintain to schedule the time window of your client's heartbeats.