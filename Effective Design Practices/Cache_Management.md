# Cache Management

- Some Web Frameworks such as Spring Boot provides method-level "caching" using the @Cacheable primitive
- The way this "method-level" works is that, the results of the method is stored when you call the method, the first time around.
- Be careful only to cache good values and not error conditions.
- Also, do not try to do cache management beyond this implicit caching, for refreshing the data in the cache.
- Instead, if you need advanced cache management, use a standard cache manager instead of writing from scratch.
