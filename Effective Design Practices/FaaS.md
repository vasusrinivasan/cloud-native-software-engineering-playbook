# Function as a Service

Use a Function as a Service (Faas) such as Fission in your Kubernetes deployment, instead of a server process for the following use cases:
- For serving static web pages
- For a periodically scheduled batch job
- For an ELT Task (Extract, Load, Transform)
