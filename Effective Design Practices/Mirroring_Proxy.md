# Mirror Proxy for Web Hooks

- Use Envoy Proxy as a Front proxy to mirror to multiple internal web services endpoints.

- This is particularly useful for situations when your services are consumers to web hooks.
- Instead of having multiple web hook URLs, you could provide one URL and mirror to multiple service endpoints through the Envoy Proxy.
