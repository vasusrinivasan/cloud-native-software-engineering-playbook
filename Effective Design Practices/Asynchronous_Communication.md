# Choose Asynchrounous communication as a backup strategy for internal services (when you need one)

- Instead of having a synchronous mode of communication between microservices, choose an asynchronous mode of communication between microservices.

- You could use Kafka as an asynchronous communication platform and create topics for different microservices.

- Whenever a microservice wants to communicate with another, it will call the other microservice using gRPC directly.
- All these gRPC microservices have a timeout and if the timeout happens, then the caller gives up and puts it in the Kafka Topic, which will be consumed as soon as it is available by the callee microservice.
- This backup strategy is needed for complex service meshes and not for simple service mesh architecture.
