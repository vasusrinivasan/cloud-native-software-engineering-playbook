Server Sent Events Streaming (SSE)

Choose Server Sent Events (SSE) Streaming over Web Sockets for Real-time delivery of events. 

- SSE is an HTTP technique for one-way communication and is more suitable than Web Sockets as it is two-way which is not required in most cases.

- Use Web Sockets only in circumstances when your clients do not have the context to initiate streaming using server-sent events (SSE).