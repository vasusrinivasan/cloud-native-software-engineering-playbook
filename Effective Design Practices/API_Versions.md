# Version APIs for breaking changes

Breaking changes include:
- Format changes of the response data for one or more calls.
- Response or request type changes.
- Removing part(s) of the API.

Breaking changes must result in versioning of the API.

- The commonly used and most straightforward approach to incorporating versioning in an API is to use the URI to denote the version of the API. 

- For instance:  https://api.acme.org/v1/endpoint1 is an example of version 1 of the API.

- If there are non-breaking changes, no API versioning is necessary.
