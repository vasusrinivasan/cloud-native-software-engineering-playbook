# Access Token
- Secure all web service endpoints with an access token which should be passed as a Bearer-Token HTTP Header in the HTTP Request.
- Clients must use a get_token endpoint HTTP GET request by passing a Basic Auth credentials using "Authorization HTTP Header" request.
- These access tokens should expire every hour and therefore all clients must call the get_token endpoint every hour to get a new access_otken using a new get_token endpoint.
