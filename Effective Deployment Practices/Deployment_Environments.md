# Deployment Environments

Your microservices need to be deployed at a minimum to the following deployment environments:

- Development environment, which has the code corresponding to a future release. As the name signifies this environment is used by Development to deploy their unit tested code to integrate with other microservices.

- QA environment, which has the code corresponding to a future release that is considered code complete after it works in the Development environment. As the name signifies this environment is used by the QA team to test the deployment that has been declared as code complete.

- Pre-Production environment, which has the code corresponding to a soon-to-be deployed release or patch to the production environment and which has the data in databases and other software components identical to the production environment. Code that is certified by the QA team as working, must be elevated to the Pre-production environment and tested again, before elevating to the production environment.

- Production environment, which has the code corresponding to the current release, used by users.
