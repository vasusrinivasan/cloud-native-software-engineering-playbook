# Kubernetes Namespaces

Create Kubernetes namespaces for the different deployment environments

- Use Kubernetes namespaces for different deployments, such as ns-acme-dv for development, ns-acme-qa for QA, ns-acme-pp for PP and ns-acme-pr for Production environment.

- Have different access policies (RBAC) for different namespaces to protect unintentional and illegal access by users.

