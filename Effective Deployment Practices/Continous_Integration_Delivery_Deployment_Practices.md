# Continuous Integration, Continuous Delivery Or Continuous Deployment

You have to make a choice to use either Continuous Delivery or Continuous Deployment as a deployment strategy.

- Continuous Integration is a development practice that requires developers to integrate code into a shared repository several times a day. Every time the software’s code is changed, it is built and tested automatically. This practice should be considered as an imperative for your software development organization. 

- Continuous Delivery is when the deployment image is built and is made available.

- Continuous Deployment is when the delivered image is automatically deployed to the production environment. 

- In other words, in your CI/CD pipeline, if the deployment stage is manual, then you are practicing continuous delivery and not continuous deployment.

- Based on your time-to-market requirements, context, as well as the maturity of your testing practices, decide whether you would use Continuous Delivery or Continuous Deployment.
