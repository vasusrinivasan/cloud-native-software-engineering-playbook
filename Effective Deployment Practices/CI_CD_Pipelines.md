# CI/CD Pipeline Stages

Your CI/CD pipeline needs to have the following stages:

- Compile, Unit Test, Test Coverage and Build Stage. In this stage you don't merely compile and build, but also run the unit tests and measure test coverage.
- If your test coverage shows that the tests are below 30% or whatever you have configured, the build should be failed.
- If the test coverage succeeds, the docker image needs to be built and pushed to the Docker Registry.

- Security Vulnerability detection stage: In this stage a security vulnerability detection software is run on the built docker image. The security vulnerability detection software lists the detected vulnerabilities by severity.
- If the pipeline is triggered due to a checkin in the dev branch, automatic Issues are created using JIRA or the used issue management software for the different vulnerabilities.
- If the pipeline is triggered due to a checkin in the releases or master branches, then the stage will be passed but will not trigger the next stage automatically. However, the next stage can be manually triggered.

- REST Calls/Integration Testing stage: This stage will use the newman scripts in the repository and test all the REST calls in the collection.

- Deployment stage: The docker images are deployed to the respective envrionments either manually or automatically in this stage, depending on whether you choose continuous delivery or continuous deployment strategy for your organization.


