# Change Request/Issue Tracking Process 

The following transitions are recommended in your Issue Tracking Tool.
- Open
- In Progress
- Ready for QA
- QA Failed
- Closed 

- When a Change Request is created, it is set to ‘Open’.

- When it is assigned to someone, the status is set to ‘In Progress’.

- When it is completed by a developer, the status is set to ‘Ready for QA’.

- When a tester certifies as completed, the status is set to ‘Closed’.

- When a tester finds it is not working, the status is set to ‘QA Failed’ and assigned back to the developer for a fix.
