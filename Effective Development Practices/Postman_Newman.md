# Postman and Newman collections in your repository.

- If you are using Postman to test your REST APIs, then checkin your Postman collection to the source code repository.

- Also run your postman collection as a script using Newman, during deployment.