# Merge Request Process

The following describes the best practice for an effective merge request (MR) or Pull Request(PR) Process.

- After completing the development and unit testing of a feature, a developer submits a merge request to submit their change request branch either to the dev branch or the patch release branch as relevant.

- The development team reviews the code changes, makes comments, in response to which the developer makes the corresponding code changes for the comments and the merge/pull request is either approved or rejected for merge.

- Once the PR or MR is approved, these “change request” branches would be automatically deleted when a merge to the dev branch or the patch release branch is completed. 

- Further more, when the cluster of change requests corresponding to functional changes are completed, a merge request is created to merge the code changes to the appropriate release branch to be validated by the QA team, in the QA environment.

- When code is ready for release and to be deployed in production, a merge/pull request is created to merge from the release branch to the master branch.
