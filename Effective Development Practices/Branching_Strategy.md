# Source code branching strategy

Source code in a source code management system such as Git must have the following branches:

- dev branch which has code that would be deployed to the development environment.

- Several release branches corresponding to the different product releases.

- master branch which has code that is currently running in production.

- Several developer “change request” branches which have the same name as the change request in an issue tracking system such as JIRA. 

- Typically a developer works on a future release or a patch release in production pertaining to a change request.

- Typically branch merges happen from the change request branch to dev branch, if it is a future release or to a patch release branch, if it is a production patch.

- From the dev branch, changes go to the appropriate release branch which would be subsequently merged to the master during release to production.
